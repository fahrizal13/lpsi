<%
    String id = request.getParameter("id");
    String ubah = request.getParameter("ubah");
    String hapus = request.getParameter("hapus");
    String ubah_simpan = request.getParameter("ubah_simpan");
    String tambah = request.getParameter("tambah");
    String tambah_simpan = request.getParameter("tambah_simpan");
    
    if (hapus !=null) {
    PreparedStatement pst = conn.prepareStatement("UPDATE jadwal_jenis SET deleted=1 WHERE id=?");
    
    pst.setString(1,id);
    pst.executeUpdate();       
    
    }
    
    if (ubah_simpan !=null) {
        
        String ubah_id = request.getParameter("ubah_id");
        String ubah_nama = request.getParameter("nama");
      
      
    PreparedStatement pst = conn.prepareStatement("UPDATE jadwal_jenis SET jenis=? WHERE id=?");
    
    pst.setString(1,ubah_nama);
    pst.setString(2,ubah_id);
    pst.executeUpdate();       
    
    }
    
    if (tambah_simpan !=null) {
        
        String ubah_nama = request.getParameter("nama");
      
      
    PreparedStatement pst = conn.prepareStatement("INSERT INTO jadwal_jenis SET jenis=?, deleted=0 ");
    
    pst.setString(1,ubah_nama);
    pst.executeUpdate();       
    
    }
    
    if (ubah !=null) {
        
    PreparedStatement pstu = conn.prepareStatement("SELECT * FROM jadwal_jenis WHERE id=?");
    pstu.setString(1,id);
    ResultSet rsu = pstu.executeQuery();
    
                                           
%>
               <!-- page content -->
            <div class="right_col" role="main">
                <div class="">

                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Ubah Jenis Kegiatan</h2>
                                    
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <br />
                                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="POST" action="">

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama<span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <%
                                                    while (rsu.next()) {
                                                        %>
                                                    
                                                <input type="text" id="first-name" name="nama" required="required" value="<% out.println(rsu.getString(2)); %>" class="form-control col-md-7 col-xs-12">
                                            <% }%>
                                            </div>
                                        </div>
                                        
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                <input type="hidden" name="ubah_id" value="<% out.println(id); %>" />
                                                <button type="submit" name="ubah_simpan" class="btn btn-success">Ubah</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /page content -->
<%           
    }
else if (tambah !=null) {
        
                                           
%>
               <!-- page content -->
            <div class="right_col" role="main">
                <div class="">

                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Tambah Jenis Kegiatan</h2>
                                    
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <br />
                                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="POST" action="">

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama<span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                    
                                                <input type="text" id="first-name" name="nama" required="required" class="form-control col-md-7 col-xs-12">
                                            
                                            </div>
                                        </div>
                                        
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                           
                                                <button type="submit" name="tambah_simpan" class="btn btn-success">Tambah</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /page content -->
<%           
    }
else 
{
%>

<!-- page content -->
            <div class="right_col" role="main">

                <div>
                    <!--<div class="page-title">
                        <div class="title_left">
                            <h3>Jenis Kegiatan</h3>
                        </div>

                        <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                                </div>
                            </div>
                        </div>
                    </div>-->
                    <div class="clearfix"></div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Jenis Kegiatan</h2>
                                                                        <ul class="nav navbar-right panel_toolbox">
                                        <li>
                                                        <form method="post" action="">
                                                            <button name="tambah" type="submit" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i> Tambah </button>
                                                        </form>
                                        </li>
                                    </ul>

                                    <!--<ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="#">Settings 1</a>
                                                </li>
                                                <li><a href="#">Settings 2</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>-->
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">

                                    <!-- start project list -->
                                    <table class="table table-striped projects">
                                        <thead>
                                            <tr>
                                                <th style="width: 1%">No.</th>
                                                <th style="width: 20%">Jenis Kegiatan</th>
                                                <th style="width: 20%">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                             <%
                                                 int i = 0;
                                             
                                            PreparedStatement pst = conn.prepareStatement ("SELECT * FROM jadwal_jenis WHERE deleted =0");
                                            ResultSet rs = pst.executeQuery();
                                            
                                            while(rs.next())
                                            { 
                                                i++;
                                            %>
                                            <tr>
                                                <td><% out.println(i); %></td>
                                                <td>
                                                    <a><% out.println(rs.getString(2)); %></a>
                                                    
                                                </td>
                                                <td>
                                                    <table>
                                                            <tr>
                                                                <td>
                                                        <form method="post" action="">
                                                            <input type="hidden" name="id" value ="<% out.println(rs.getString(1)); %>" />
                                                            <button name="ubah" type="submit" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Ubah </button>
                                                        </form>
                                                                </td>
                                                                <td>
                                                        <form method="post" action="">
                                                            <input type="hidden" name="id" value ="<% out.println(rs.getString(1)); %>" />
                                                            <button name="hapus" type="submit" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Hapus </button>
                                                        </form>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                </td>
                                            </tr>
                                            <%
                                                }
                                            %>
                                             </tbody>
                                    </table>
                                    <!-- end project list -->

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                                             <%
                                                 }
                                             %>