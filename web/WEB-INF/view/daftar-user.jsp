<%
    String id = request.getParameter("id");
    String ubah = request.getParameter("ubah");
    String hapus = request.getParameter("hapus");
    String tambah = request.getParameter("tambah");
    String tambah_simpan = request.getParameter("tambah_simpan");
    String ubah_simpan = request.getParameter("ubah_simpan");
    
    if (hapus !=null) {
    PreparedStatement pst = conn.prepareStatement("UPDATE user SET deleted=1 WHERE id=?");
    
    pst.setString(1,id);
    pst.executeUpdate();       
    
    }
    
    if (ubah_simpan !=null) {
        
        String ubah_id = request.getParameter("ubah_id");
        String ubah_nama = request.getParameter("nama");
        String ubah_pekerjaan = request.getParameter("pekerjaan");
        String ubah_username = request.getParameter("username");
        String ubah_email = request.getParameter("email");
        String ubah_autoritas = request.getParameter("autoritas");
      
      
    PreparedStatement pst = conn.prepareStatement("UPDATE user SET nama=?, pekerjaan=?, username=?, email=?, autoritas=? WHERE id=?");
    
    pst.setString(1,ubah_nama);
    pst.setString(2,ubah_pekerjaan);
    pst.setString(3,ubah_username);
    pst.setString(4,ubah_email);
    pst.setString(5,ubah_autoritas);
    pst.setString(6,ubah_id);
    pst.executeUpdate();       
    
    }
    
    
    if (tambah_simpan !=null) {
    
        String tambah_foto = "default.png";
        String tambah_nama = request.getParameter("nama");
        String tambah_pekerjaan = request.getParameter("pekerjaan");
        String tambah_username = request.getParameter("username");
        String tambah_email = request.getParameter("email");
        String tambah_password = request.getParameter("password");
        String tambah_autoritas = request.getParameter("autoritas");
        String tambah_waktu = now;
      
//   out.println(tambah_nama+"--"+tambah_pekerjaan+"--"+tambah_username+"--"+tambah_email+"--"+tambah_autoritas);      
    PreparedStatement pst = conn.prepareStatement("INSERT INTO user SET foto=?, nama=?, pekerjaan=?, username=?, email=?, password=?, autoritas=?, waktu=?, deleted=0");
    
    pst.setString(1,tambah_foto);
    pst.setString(2,tambah_nama);
   pst.setString(3,tambah_pekerjaan);
    pst.setString(4,tambah_username);
    pst.setString(5,tambah_email);
    pst.setString(6,tambah_password);
    pst.setString(7,tambah_autoritas);
    pst.setString(8,tambah_waktu);
    pst.executeUpdate();       
    
    }
    
    if (ubah !=null) {
        
    PreparedStatement pstu = conn.prepareStatement("SELECT * FROM user WHERE id=?");
    pstu.setString(1,id);
    ResultSet rsu = pstu.executeQuery();
    
                                           
%>
               <!-- page content -->
            <div class="right_col" role="main">
                <div class="">

                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Ubah Profile User</h2>
                                    
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <br />
                                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="POST" action="">
    <%
                                                    rsu.next();
                                                        %>
                                            
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama<span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                    
                                                <input type="text" id="first-name" name="nama" required="required" value="<% out.println(rsu.getString(3)); %>" class="form-control col-md-7 col-xs-12">
                                            
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Pekerjaan<span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                               
                                                <input type="text" id="first-name" name="pekerjaan" required="required" value="<% out.println(rsu.getString(4)); %>" class="form-control col-md-7 col-xs-12">
                                            
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Username<span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="first-name" name="username" required="required" value="<% out.println(rsu.getString(5)); %>" class="form-control col-md-7 col-xs-12">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">E-mail<span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="first-name" name="email" required="required" value="<% out.println(rsu.getString(6)); %>" class="form-control col-md-7 col-xs-12">
                                            </div>
                                        </div>
                                            
                                            <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Autoritas</label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                <select class="form-control" name="autoritas">
                                                <%
    PreparedStatement psta = conn.prepareStatement("SELECT * FROM user_autoritas ORDER BY jenis_autoritas ASC ");
    ResultSet rsa = psta.executeQuery();
                                                    while (rsa.next()) {
                                                        
                                                    if(rsu.getString(8).equals(rsa.getString(1))) {
                                                        %>
                                                    
                                                    <option selected="selected" value="<% out.println(rsa.getString(1)); %>"><% out.println(rsa.getString(2)); %></option>
                                                    <%
                                                        
                                                    } else {
                                                    %>
                                                    <option value="<% out.println(rsa.getString(1)); %>"><% out.println(rsa.getString(2)); %></option>
                                            <% } 
} %>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                <input type="hidden" name="ubah_id" value="<% out.println(id); %>" />
                                                <button type="submit" name="ubah_simpan" class="btn btn-success">Ubah</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /page content -->
<%           
    }
else if (tambah !=null) {
        
    
                                           
%>
               <!-- page content -->
            <div class="right_col" role="main">
                <div class="">

                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Tambah Profile User</h2>
                                    
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <br />
                                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="POST" action="">

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama<span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                    
                                                <input type="text" id="first-name" name="nama" required="required" class="form-control col-md-7 col-xs-12">
                                            
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Pekerjaan<span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                               
                                                <input type="text" id="first-name" name="pekerjaan" required="required" class="form-control col-md-7 col-xs-12">
                                            
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Username<span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="first-name" name="username" required="required" class="form-control col-md-7 col-xs-12">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Password<span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="first-name" name="password" required="required" class="form-control col-md-7 col-xs-12">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">E-mail<span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="first-name" name="email" required="required" class="form-control col-md-7 col-xs-12">
                                            </div>
                                        </div>
                                            
                                       
                                            <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Autoritas</label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                <select class="form-control" name="autoritas">
                                                    <option>Pilih Autoritas</option>
                                                <%
    PreparedStatement psta = conn.prepareStatement("SELECT * FROM user_autoritas ORDER BY jenis_autoritas ASC ");
    ResultSet rsa = psta.executeQuery();
                                                    while (rsa.next()) {
                                                        %>
                                                    
                                                    <option value="<% out.println(rsa.getString(1)); %>"><% out.println(rsa.getString(2)); %></option>
                                            <% }%>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                  
                                                <button type="submit" name="tambah_simpan" class="btn btn-success">tambah</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /page content -->
<%           
    } 

else 
{
%>

<!-- page content -->
            <div class="right_col" role="main">

                <div class="">
                    <div class="page-title">
                        <div class="title_left">
                            <h3>Daftar User</h3>
                        </div>

                        <div class="title_right">
                             <ul class="nav navbar-right panel_toolbox">
                                        <li>
                                                        <form method="post" action="">
                                                            <button name="tambah" type="submit" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i> Tambah </button>
                                                        </form>
                                        </li>
                                    </ul>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="x_panel">
                                <div class="x_content">

                                    <div class="row">

                                        <div class="clearfix"></div>
                                        
                                            <%
                                            PreparedStatement pst = conn.prepareStatement ("SELECT * FROM user WHERE deleted =0");
                                            ResultSet rs = pst.executeQuery();
                                            
                                            while(rs.next())
                                            { 
                                                
                                            %>
                                            
                                        <div class="col-md-4 col-sm-4 col-xs-12 animated fadeInDown">
                                            <div class="well profile_view">
                                                <div class="col-sm-12">
                                                    <!--<h4 class="brief"><i>--</i></h4>-->
                                                    <div class="left col-xs-7">
                                                        <h2><% out.println(rs.getString(3)); %></h2>
                                                        <p><% out.println(rs.getString(4)); %></p>
                                                        
                                                    </div>
                                                    <div class="right col-xs-5 text-center">
                                                        <!--<img src="uploads/user/<% out.println(rs.getString(2)); %>" alt="" class="img-circle img-responsive">-->
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 bottom text-center">
                                                   
                                                    <div class="col-xs-12 col-sm-6 emphasis">
                                                      
                                                    <table>
                                                            <tr>
                                                                <td>
                                                        <form method="post" action="">
                                                            <input type="hidden" name="id" value ="<% out.println(rs.getString(1)); %>" />
                                                            <button name="ubah" type="submit" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Ubah </button>
                                                        </form>
                                                                </td>
                                                                <td>
                                                        <form method="post" action="">
                                                            <input type="hidden" name="id" value ="<% out.println(rs.getString(1)); %>" />
                                                            <button name="hapus" type="submit" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Hapus </button>
                                                        </form>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                            
                                            <%
                                            }
                                            %>
                                        
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                                            
                                             <%
                                                 }
                                             %>