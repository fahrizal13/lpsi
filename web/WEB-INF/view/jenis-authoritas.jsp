<%
    String id = request.getParameter("id");
    String ubah = request.getParameter("ubah");
    String tambah = request.getParameter("tambah");
    String hapus = request.getParameter("hapus");
    String ubah_simpan = request.getParameter("ubah_simpan");
    String tambah_simpan = request.getParameter("tambah_simpan");
  
    if (tambah_simpan !=null) {
        
        String tambah_jenis_autoritas = request.getParameter("nama");
        String tambah_jadwal_approve = request.getParameter("jadwal_approve");
        String tambah_user_management = request.getParameter("user_management");

        PreparedStatement stx = conn.prepareStatement("INSERT INTO user_autoritas SET jenis_autoritas='"+tambah_jenis_autoritas+"', user_management='"+tambah_user_management+"', jadwal_approve='"+tambah_jadwal_approve+"', deleted=0 ");
        stx.executeUpdate(); 
    }
    
    if (hapus !=null) {
    PreparedStatement pst = conn.prepareStatement("UPDATE user_autoritas SET deleted=1 WHERE id=?");
    
    pst.setString(1,id);
    pst.executeUpdate();       
    
    }
    if (ubah_simpan !=null) {
        
        String ubah_id = request.getParameter("ubah_id");
        String ubah_nama = request.getParameter("nama");
        String jadwal_approve = request.getParameter("jadwal_approve");
        String user_management = request.getParameter("user_management");
      
    PreparedStatement pst = conn.prepareStatement("UPDATE user_autoritas SET jenis_autoritas=?, jadwal_approve=?, user_management=? WHERE id=?");
    
    pst.setString(1,ubah_nama);
    pst.setString(2,jadwal_approve);
    pst.setString(3,user_management);
    pst.setString(4,ubah_id);
    pst.executeUpdate();       
    
    }
    
    if (ubah !=null) {
        
    PreparedStatement pstu = conn.prepareStatement("SELECT * FROM user_autoritas WHERE id=?");
    pstu.setString(1,id);
    ResultSet rsu = pstu.executeQuery();
    rsu.next();
                                           
%>
               <!-- page content -->
            <div class="right_col" role="main">
                <div class="">

                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Ubah Autoritas</h2>
                                    
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <br />
                                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="POST" action="">

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama<span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                    
                                                <input type="text" id="first-name" name="nama" required="required" value="<% out.println(rsu.getString(2)); %>" class="form-control col-md-7 col-xs-12">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Manajemen Kegiatan</label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <div id="gender" class="btn-group" data-toggle="buttons">
                                                    <label class="btn btn-default <% if(rsu.getString(3).equals("1")) { %> active <% } %>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                                        <input <% if(rsu.getString(3).equals("1")) { %> checked="checked" <% } %> type="radio" name="jadwal_approve" value="1"> &nbsp; Aktif &nbsp;
                                                    </label>
                                                    <label class="btn btn-default <% if(rsu.getString(3).equals("0")) { %> active <% } %>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                                        <input <% if(rsu.getString(3).equals("0")) { %> checked="checked" <% } %> type="radio" name="jadwal_approve" value="0"> Tidak Aktif
                                                    </label>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Manajemen User</label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <div id="gender" class="btn-group" data-toggle="buttons">
                                                    <label class="btn btn-default <% if(rsu.getString(4).equals("1")) { %> active <% } %>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                                        <input <% if(rsu.getString(4).equals("1")) { %> checked="checked" <% } %> type="radio" name="user_management" value="1"> &nbsp; Aktif &nbsp;
                                                    </label>
                                                    <label class="btn btn-default <% if(rsu.getString(4).equals("0")) { %> active <% } %>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                                        <input <% if(rsu.getString(4).equals("0")) { %> checked="checked" <% } %> type="radio" name="user_management" value="0"> Tidak Aktif
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                <input type="hidden" name="ubah_id" value="<% out.println(id); %>" />
                                                <button type="submit" name="ubah_simpan" class="btn btn-success">Ubah</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /page content -->
<%           
    }
else if (tambah !=null) {
        
                                         
%>
               <!-- page content -->
            <div class="right_col" role="main">
                <div class="">

                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Tambah Autoritas</h2>
                                    
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <br />
                                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="POST" action="">

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama<span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                    
                                                <input type="text" id="first-name" name="nama" required="required" class="form-control col-md-7 col-xs-12">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Manajemen Kegiatan</label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <div id="gender" class="btn-group" data-toggle="buttons">
                                                    <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                                        <input type="radio" name="jadwal_approve" value="1"> &nbsp; Aktif &nbsp;
                                                    </label>
                                                    <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                                        <input type="radio" name="jadwal_approve" value="0"> Tidak Aktif
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Manajemen User</label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <div id="gender" class="btn-group" data-toggle="buttons">
                                                    <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                                        <input type="radio" name="user_management" value="1"> &nbsp; Aktif &nbsp;
                                                    </label>
                                                    <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                                        <input type="radio" name="user_management" value="0"> Tidak Aktif
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                <button type="submit" name="tambah_simpan" class="btn btn-success">Tambah</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /page content -->
<%           
    }
else 
{
%>

<!-- page content -->
            <div class="right_col" role="main">

                <div>
                    <!--<div class="page-title">
                        <div class="title_left">
                            <h3>Jenis Kegiatan</h3>
                        </div>

                        <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                                </div>
                            </div>
                        </div>
                    </div>-->
                    <div class="clearfix"></div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Jenis Authoritas</h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li>
                                                        <form method="post" action="">
                                                            <button name="tambah" type="submit" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i> Tambah </button>
                                                        </form>
                                        </li>
                                    </ul>
                                    <!--<ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="#">Settings 1</a>
                                                </li>
                                                <li><a href="#">Settings 2</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>-->
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">

                                    <!-- start project list -->
                                    <table class="table table-striped projects">
                                        <thead>
                                            <tr>
                                                <th style="width: 1%">No.</th>
                                                <th style="width: 20%">Nama Autoritas</th>
                                                <th style="width: 20%">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                             <%
                                                 int i = 0;
                                             
                                            PreparedStatement pst = conn.prepareStatement ("SELECT * FROM user_autoritas WHERE deleted =0");
                                            ResultSet rs = pst.executeQuery();
                                            
                                            while(rs.next())
                                            { 
                                                i++;
                                            %>
                                            <tr>
                                                <td><% out.println(i); %></td>
                                                <td>
                                                    <a><% out.println(rs.getString(2)); %></a>
                                                    
                                                </td>
                                                <td>
                                                    <table>
                                                            <tr>
                                                                <td>
                                                        <form method="post" action="">
                                                            <input type="hidden" name="id" value ="<% out.println(rs.getString(1)); %>" />
                                                            <button name="ubah" type="submit" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Ubah </button>
                                                        </form>
                                                                </td>
                                                                <td>
                                                        <form method="post" action="">
                                                            <input type="hidden" name="id" value ="<% out.println(rs.getString(1)); %>" />
                                                            <button name="hapus" type="submit" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Hapus </button>
                                                        </form>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                </td>
                                            </tr>
                                            <%
                                                }
                                            %>
                                             </tbody>
                                    </table>
                                    <!-- end project list -->

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                                             <%
                                                 }
                                             %>