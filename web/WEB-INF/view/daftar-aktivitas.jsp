            <!-- page content -->
            <div class="right_col" style="min-height:600px;" role="main">

                <br />
                <div class="">

                    
                    <div class="row">
                        <div class="col-md-4">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Permintaan Kegiatan</h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    
                                             <%
                                                 int i = 0;
                                             
                                            PreparedStatement pst = conn.prepareStatement ("SELECT * FROM jadwal WHERE deleted =0 AND status=0 ORDER BY start DESC LIMIT 3");
                                            ResultSet rs = pst.executeQuery();
                                            
                                            while(rs.next())
                                            { 
                                                i++;
    SimpleDateFormat dateFormata1 = new SimpleDateFormat("MMM");
    Timestamp formata1 = rs.getTimestamp(6);
    String datea1 = dateFormata1.format(formata1);
    
    SimpleDateFormat dateFormata2 = new SimpleDateFormat("dd");
    Timestamp formata2 = rs.getTimestamp(6);
    String datea2 = dateFormata2.format(formata2);
                                            %>
                                            
                                    <article class="media event">
                                        <a class="pull-left date">
                                            <p class="month"><% out.println(datea1); %></p>
                                            <p class="day"><% out.println(datea2); %></p>
                                        </a>
                                        <div class="media-body">
                                            <a class="title" href="#"><% out.println(rs.getString(3)); %></a>
                                            <p><% out.println(rs.getString(5)); %></p>
                                            <hr>
                                            <p><small>Dari <b><% out.println(rs.getString(6)); %></b> sampai <b><% out.println(rs.getString(7)); %></b></small></p>
                                        </div>
                                    </article>
                                        <hr>
                                            <%
                                                }
                                            %>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Pengguna Baru</h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <%
                                            int j = 0;
                                             
                                            PreparedStatement pstj = conn.prepareStatement ("SELECT * FROM user WHERE deleted =0 ORDER BY waktu DESC LIMIT 5");
                                            ResultSet rsj = pstj.executeQuery();
                                            
                                            while(rsj.next())
                                            { 
                                                j++;
    SimpleDateFormat dateFormata1 = new SimpleDateFormat("MMM");
    Timestamp formata1 = rsj.getTimestamp(9);
    String datea1 = dateFormata1.format(formata1);
    
    SimpleDateFormat dateFormata2 = new SimpleDateFormat("dd");
    Timestamp formata2 = rsj.getTimestamp(9);
    String datea2 = dateFormata2.format(formata2);
                                            %>
                                            
                                    <article class="media event">
                                        <a class="pull-left date">
                                            <p class="month"><% out.println(datea1); %></p>
                                            <p class="day"><% out.println(datea2); %></p>
                                        </a>
                                        <div class="media-body">
                                            <a class="title" href="#"><% out.println(rsj.getString(3)); %> </a> <% out.println(rsj.getString(4)); %>
                                            
                                            <p><small>Bergabung pada <b><% out.println(rsj.getString(9)); %></b></small></p>
                                        </div>
                                    </article>
                                        <hr>
                                            <%
                                                }
                                            %>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>File Terbaru</h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                                                        <%
                                            int k = 0;
                                             
                                            PreparedStatement pstk = conn.prepareStatement ("SELECT * FROM share_file WHERE deleted =0 ORDER BY waktu DESC LIMIT 5");
                                            ResultSet rsk = pstk.executeQuery();
                                            
                                            while(rsk.next())
                                            { 
                                                k++;
    SimpleDateFormat dateFormata1 = new SimpleDateFormat("MMM");
    Timestamp formata1 = rsk.getTimestamp(7);
    String datea1 = dateFormata1.format(formata1);
    
    SimpleDateFormat dateFormata2 = new SimpleDateFormat("dd");
    Timestamp formata2 = rsk.getTimestamp(7);
    String datea2 = dateFormata2.format(formata2);
                                            %>
                                            
                                    <article class="media event">
                                        <a class="pull-left date">
                                            <p class="month"><% out.println(datea1); %></p>
                                            <p class="day"><% out.println(datea2); %></p>
                                        </a>
                                        <div class="media-body">
                                            <a class="title" href="#"><% out.println(rsk.getString(3)); %></a>
                                            <p><% out.println(rsk.getString(6)); %></p>
                                            <p><small>Diunggah pada <b><% out.println(rsk.getString(7)); %></b></small></p>
                                        </div>
                                    </article>
                                        <hr>
                                            <%
                                                }
                                            %>
                                </div>
                            </div>
                        </div>
                    </div>
                                
                    <div class="row">
                        
                        <div class="col-md-4">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Kegiatan Terdekat</h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <%
                                            int m = 0;
                                             
                                            PreparedStatement pstm = conn.prepareStatement ("SELECT * FROM jadwal WHERE deleted =0 ORDER BY start DESC LIMIT 3");
                                            ResultSet rsm = pstm.executeQuery();
                                            
                                            while(rsm.next())
                                            { 
                                                m++;
    SimpleDateFormat dateFormata1 = new SimpleDateFormat("MMM");
    Timestamp formata1 = rsm.getTimestamp(6);
    String datea1 = dateFormata1.format(formata1);
    
    SimpleDateFormat dateFormata2 = new SimpleDateFormat("dd");
    Timestamp formata2 = rsm.getTimestamp(6);
    String datea2 = dateFormata2.format(formata2);
                                            %>
                                            
                                    <article class="media event">
                                        <a class="pull-left date">
                                            <p class="month"><% out.println(datea1); %></p>
                                            <p class="day"><% out.println(datea2); %></p>
                                        </a>
                                        <div class="media-body">
                                            <a class="title" href="#"><% out.println(rsm.getString(3)); %></a>
                                            <p><% out.println(rsm.getString(5)); %></p>
                                           
                                            <hr>
                                            <p><small>Dari <b><% out.println(rsm.getString(6)); %></b> sampai <b><% out.println(rsm.getString(7)); %></b></small></p>
                                        </div>
                                    </article>
                                        <hr>
                                            <%
                                                }
                                            %>
                                </div>
                            </div>
                        </div>
                                
                        <div class="col-md-4">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Session Pengguna</h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    
                                             <%
                                                 int l = 0;
                                             
                                            PreparedStatement pstl = conn.prepareStatement ("SELECT * FROM user_session ORDER BY waktu DESC LIMIT 5");
                                            ResultSet rsl = pstl.executeQuery();
                                            
                                            while(rsl.next())
                                            { 
                                                l++;
                                            PreparedStatement pstlz = conn.prepareStatement ("SELECT * FROM user WHERE id=?");
    pstlz.setString(1,rsl.getString(3));
                                            ResultSet rslz = pstlz.executeQuery();
                                            rslz.next();
                                            
    SimpleDateFormat dateFormata1 = new SimpleDateFormat("MMM");
    Timestamp formata1 = rsl.getTimestamp(4);
    String datea1 = dateFormata1.format(formata1);
    
    SimpleDateFormat dateFormata2 = new SimpleDateFormat("dd");
    Timestamp formata2 = rsl.getTimestamp(4);
    String datea2 = dateFormata2.format(formata2);
                                            %>
                                            
                                    <article class="media event">
                                        <a class="pull-left date">
                                            <p class="month"><% out.println(datea1); %></p>
                                            <p class="day"><% out.println(datea2); %></p>
                                        </a>
                                        <div class="media-body">
                                            <a class="title" href="#"><% out.println(rslz.getString(3)); %></a> telah <b><% out.println(rsl.getString(2)); %></b>
                                            <p><small>Pada <b><% out.println(rsl.getString(4)); %></b></small></p>
                                        </div>
                                    </article>
                                        <hr>
                                            <%
                                                }
                                            %>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Folder Terbaru</h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                                                        <%
                                            int n = 0;
                                             
                                            PreparedStatement pstn = conn.prepareStatement ("SELECT * FROM share_folder WHERE deleted =0 ORDER BY waktu DESC LIMIT 5");
                                            ResultSet rsn = pstn.executeQuery();
                                            
                                            while(rsn.next())
                                            { 
                                                n++;
    SimpleDateFormat dateFormata1 = new SimpleDateFormat("MMM");
    Timestamp formata1 = rsn.getTimestamp(4);
    String datea1 = dateFormata1.format(formata1);
    
    SimpleDateFormat dateFormata2 = new SimpleDateFormat("dd");
    Timestamp formata2 = rsn.getTimestamp(4);
    String datea2 = dateFormata2.format(formata2);
                                            %>
                                            
                                    <article class="media event">
                                        <a class="pull-left date">
                                            <p class="month"><% out.println(datea1); %></p>
                                            <p class="day"><% out.println(datea2); %></p>
                                        </a>
                                        <div class="media-body">
                                            <a class="title" href="#"><% out.println(rsn.getString(2)); %></a>
                                            <p><small>Telah dibuat pada <b><% out.println(rsn.getString(4)); %></b></small></p>
                                           
                                        </div>
                                    </article>
                                            <hr>
                                            <%
                                                }
                                            %>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>