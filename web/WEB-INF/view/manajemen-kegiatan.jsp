<%
    String id = request.getParameter("id");
    String ubah = request.getParameter("ubah");
    String hapus = request.getParameter("hapus");
    String ubah_simpan = request.getParameter("ubah_simpan");
    String tambah = request.getParameter("tambah");
    String tambah_simpan = request.getParameter("tambah_simpan");
    
    if (hapus !=null) {
    PreparedStatement pst = conn.prepareStatement("UPDATE jadwal SET deleted=1 WHERE id=?");
    
    pst.setString(1,id);
    pst.executeUpdate();       
    
    }
    
    if (ubah_simpan !=null) {
        
        String ubah_id = request.getParameter("ubah_id");
        
        String nama = request.getParameter("nama");
        String jenis = request.getParameter("jenis");
        String deskripsi = request.getParameter("deskripsi");
        String start = request.getParameter("start");
        String finish = request.getParameter("finish");
        String status = request.getParameter("status");
      
      
        PreparedStatement pst = conn.prepareStatement("UPDATE jadwal SET nama=?, jenis=?, deskripsi=?, start=?, finish=?, status=? WHERE id=? ");

        pst.setString(1,nama);
        pst.setString(2,jenis);
        pst.setString(3,deskripsi);
        pst.setString(4,start);
        pst.setString(5,finish);
        pst.setString(6,status);
        pst.setString(7,ubah_id);
        pst.executeUpdate();    
    
    }
    
    
    if (ubah !=null) {
        
    PreparedStatement pstu = conn.prepareStatement("SELECT * FROM jadwal WHERE id=?");
    pstu.setString(1,id);
    ResultSet rsu = pstu.executeQuery();
    rsu.next();
                                           
%>
               <!-- page content -->
            <div class="right_col" role="main">
                <div class="">

                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Ubah Jadwal Kegiatan</h2>
                                    
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <br />
                                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="POST" action="">

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Status Kegiatan<span class="required">*</span>
                                            </label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                <select class="form-control" required name="status">
                                                <%
    if(rsu.getString(8).equals("1")) {
    
                                                %>
                             <option selected="selected" value="1">Telah Disetujui</option>
                             <option value="0">Belum Disetujui</option>
                                                <%
    } else {
                                                %>
                             <option value="1">Telah Disetujui</option>
                             <option selected="selected" value="0">Belum Disetujui</option>
                                                <% } %>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama<span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                    
                                                <input value="<% out.println(rsu.getString(3)); %>" type="text" id="first-name" name="nama" required="required" class="form-control col-md-7 col-xs-12">
                                            
                                            </div>
                                        </div>
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Deskripsi <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea id="textarea" required="required" name="deskripsi" class="form-control col-md-7 col-xs-12"><% out.println(rsu.getString(5)); %></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Jenis Kegiatan<span class="required">*</span>
                                            </label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                <select class="form-control" required name="jenis">
                                                    <option value="">Pilih Jenis Kegiatan</option>
                                                <%
    PreparedStatement psta = conn.prepareStatement("SELECT * FROM jadwal_jenis WHERE deleted=0 ORDER BY jenis ASC ");
    ResultSet rsa = psta.executeQuery();
                                                while (rsa.next()) {
                                                
    if(rsa.getString(1).equals(rsu.getString(4))) {
    
                                                %>
                                                <option selected="selected" value="<% out.println(rsa.getString(1)); %>"><% out.println(rsa.getString(2)); %></option>
                                                <%
    } else {
                                                %>
                                                        <option value="<% out.println(rsa.getString(1)); %>"><% out.println(rsa.getString(2)); %></option>
                                                <% } }%>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-3">Mulai*</label>
                                            <div class="col-md-9 col-sm-9 col-xs-9">
                                                <input value="<% out.println(rsu.getString(6)); %>" required name="start" type="text" class="form-control" data-inputmask="'mask': '9999-99-99 99:99:99'">
                                                <span class="fa fa-calendar form-control-feedback right" aria-hidden="true"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-3">Berakhir*</label>
                                            <div class="col-md-9 col-sm-9 col-xs-9">
                                                <input required name="finish" type="text" class="form-control" data-inputmask="'mask': '9999-99-99 99:99:99'" value="<% out.println(rsu.getString(7)); %>">
                                                <span class="fa fa-calendar form-control-feedback right" aria-hidden="true"></span>
                                            </div>
                                        </div>
                                        
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                           <input type="hidden" name="ubah_id" value="<% out.println(rsu.getString(1)); %>" />
                                                <button type="submit" name="ubah_simpan" class="btn btn-success">Ubah</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /page content -->
<%           
    }
else 
{
%>

<!-- page content -->
            <div class="right_col" role="main">

                <div>
                    <!--<div class="page-title">
                        <div class="title_left">
                            <h3>Jenis Kegiatan</h3>
                        </div>

                        <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                                </div>
                            </div>
                        </div>
                    </div>-->
                    <div class="clearfix"></div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Manajemen Kegiatan</h2>

                                    <!--<ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="#">Settings 1</a>
                                                </li>
                                                <li><a href="#">Settings 2</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>-->
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">

                                    <!-- start project list -->
                                    <table class="table table-striped projects">
                                        <thead>
                                            <tr>
                                                <th style="width: 1%">No.</th>
                                                <th style="width: 20%">Nama</th>
                                                <th style="width: 20%">Jenis</th>
                                                <th style="width: 20%">Author</th>
                                                <th style="width: 20%">Start</th>
                                                <th style="width: 20%">Finish</th>
                                                <th style="width: 20%">Status</th>
                                                <th style="width: 20%">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                             <%
                                                 int i = 0;
                                             
                                            PreparedStatement pst = conn.prepareStatement ("SELECT * FROM jadwal WHERE deleted =0 ORDER BY start DESC");
                                            ResultSet rs = pst.executeQuery();
                                            
                                            while(rs.next())
                                            { 
                                                i++;
                                            %>
                                            <tr>
                                                <td><% out.println(i); %></td>
                                                <td>
                                                    <a><% out.println(rs.getString(3)); %></a>
                                                </td>
                                                <td>
<%
    PreparedStatement pstu = conn.prepareStatement("SELECT * FROM jadwal_jenis WHERE id=?");
    pstu.setString(1,rs.getString(4));
    ResultSet rsu = pstu.executeQuery();
    rsu.next();
%>
<a><% out.println(rsu.getString(2)); %></a>
                                                </td>
                                                <td>
<%
    PreparedStatement psts = conn.prepareStatement("SELECT * FROM user WHERE id=?");
    psts.setString(1,rs.getString(2));
    ResultSet rss = psts.executeQuery();
    rss.next();
%>
                                                    <a><% out.println(rss.getString(3)); %></a>
                                                </td>
                                                <td>
                                                    <a><% out.println(rs.getString(6)); %></a>
                                                </td>
                                                <td>
                                                    <a><% out.println(rs.getString(7)); %></a>
                                                </td>
                                                <td>
                                                    <a>
                                                        <%
                                                        if(rs.getString(8).equals("1")) {
                                                            out.println("Telah Disetujui");
                                                        } else {
                                                            out.println("Belum Disetujui");
                                                        }
                                                        %>
                                                    </a>
                                                </td>
                                                <td>
                                                    <table>
                                                            <tr>
                                                                <td>
                                                        <form method="post" action="">
                                                            <input type="hidden" name="id" value ="<% out.println(rs.getString(1)); %>" />
                                                            <button name="ubah" type="submit" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Ubah </button>
                                                        </form>
                                                                </td>
                                                                <td>
                                                        <form method="post" action="">
                                                            <input type="hidden" name="id" value ="<% out.println(rs.getString(1)); %>" />
                                                            <button name="hapus" type="submit" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Hapus </button>
                                                        </form>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                </td>
                                            </tr>
                                            <%
                                                }
                                            %>
                                             </tbody>
                                    </table>
                                    <!-- end project list -->

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                                             <%
                                                 }
                                             %>