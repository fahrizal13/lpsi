<%
    String tambah = request.getParameter("tambah");
    String tambah_simpan = request.getParameter("tambah_simpan");
    
    if (tambah_simpan !=null) {
        
        String user = user_id;
        String nama = request.getParameter("nama");
        String jenis = request.getParameter("jenis");
        String deskripsi = request.getParameter("deskripsi");
        String start = request.getParameter("start");
        String finish = request.getParameter("finish");
      
      
    PreparedStatement pst = conn.prepareStatement("INSERT INTO jadwal SET user=?, nama=?, jenis=?, deskripsi=?, start=?, finish=?, status=0, deleted=0 ");
    
    pst.setString(1,user);
    pst.setString(2,nama);
    pst.setString(3,jenis);
    pst.setString(4,deskripsi);
    pst.setString(5,start);
    pst.setString(6,finish);
    pst.executeUpdate();       
    
            %>
            <script>alert("Pengajuan kegiatan Berhasil Dikirim!");</script>
            <%
    }
    
if (tambah !=null) {                                 
%>
               <!-- page content -->
            <div class="right_col" role="main">
                <div class="">

                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Tambah Jadwal Kegiatan</h2>
                                    
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <br />
                                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="POST" action="">

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama<span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                    
                                                <input type="text" id="first-name" name="nama" required="required" class="form-control col-md-7 col-xs-12">
                                            
                                            </div>
                                        </div>
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Deskripsi <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea id="textarea" required="required" name="deskripsi" class="form-control col-md-7 col-xs-12"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Jenis Kegiatan<span class="required">*</span>
                                            </label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                <select class="form-control" required name="jenis">
                                                    <option value="">Pilih Jenis Kegiatan</option>
                                                <%
    PreparedStatement psta = conn.prepareStatement("SELECT * FROM jadwal_jenis WHERE deleted=0 ORDER BY jenis ASC ");
    ResultSet rsa = psta.executeQuery();
                                                    while (rsa.next()) {
                                                        %>
                                                    
                                                    <option value="<% out.println(rsa.getString(1)); %>"><% out.println(rsa.getString(2)); %></option>
                                            <% }%>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-3">Mulai*</label>
                                            <div class="col-md-9 col-sm-9 col-xs-9">
                                                <input required name="start" type="text" class="form-control" data-inputmask="'mask': '9999-99-99 99:99:99'">
                                                <span class="fa fa-calendar form-control-feedback right" aria-hidden="true"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-3">Berakhir*</label>
                                            <div class="col-md-9 col-sm-9 col-xs-9">
                                                <input required name="finish" type="text" class="form-control" data-inputmask="'mask': '9999-99-99 99:99:99'">
                                                <span class="fa fa-calendar form-control-feedback right" aria-hidden="true"></span>
                                            </div>
                                        </div>
                                        
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                           
                                                <button type="submit" name="tambah_simpan" class="btn btn-success">Tambah</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /page content -->
<%           
}
else {
%>
                <!-- page content -->
                <div class="right_col" role="main">
                    <div class="">

                        <div class="page-title">
                            <!--<div class="title_left">
                                <h3>
                                </h3>
                            </div>

                            <div class="title_right">
                                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Search for...">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="button">Go!</button>
                                        </span>
                                    </div>
                                </div>
                            </div>-->
                        </div>
                        <div class="clearfix"></div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Jadwal Kegiatan</h2>
                                                                        <ul class="nav navbar-right panel_toolbox">
                                        <li>
                                                        <form method="post" action="">
                                                            <button name="tambah" type="submit" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i> Ajukan Kegiatan </button>
                                                        </form>
                                        </li>
                                    </ul>
                                        <!--<ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                            </li>
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="#">Settings 1</a>
                                                    </li>
                                                    <li><a href="#">Settings 2</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                                            </li>
                                        </ul>-->
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">

                                        <div id='calendar'></div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
<% } %>