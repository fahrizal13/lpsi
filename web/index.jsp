<% if(user_id == null) { response.sendRedirect("masuk"); } %>
<%
    PreparedStatement icb = conn.prepareStatement("SELECT COUNT(*) FROM jadwal WHERE status=1 AND deleted=0");
    ResultSet ricb = icb.executeQuery();
    ricb.next();
    
    PreparedStatement icc = conn.prepareStatement("SELECT COUNT(*) FROM jadwal WHERE status=0 AND deleted=0");
    ResultSet ricc = icc.executeQuery();
    ricc.next();
    
    PreparedStatement icd = conn.prepareStatement("SELECT COUNT(*) FROM user");
    ResultSet ricd = icd.executeQuery();
    ricd.next();
    
    int total_activity = ricb.getInt(1)+ricc.getInt(1)+ricd.getInt(1);
%>
            <!-- page content -->
            <div class="right_col" style="min-height:600px;" role="main">

                <br />
                <div class="">

                    <div class="row top_tiles">
                        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats">
                                <div class="icon"><i class="fa fa-sort-amount-desc"></i>
                                </div>
                                <div class="count"><% out.println(total_activity); %></div>

                                <p>Aktivitas Terkini</p>
                            </div>
                        </div>
                        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats">
                                <div class="icon"><i class="fa fa-calendar"></i>
                                </div>
                                <div class="count"><% out.println(ricb.getInt(1)); %></div>

                                <p>Kegiatan Terdekat</p>
                            </div>
                        </div>
                        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats">
                                <div class="icon"><i class="fa fa-spinner"></i>
                                </div>
                                <div class="count"><% out.println(ricc.getInt(1)); %></div>

                                <p>Jumlah Permintaan Kegiatan</p>
                            </div>
                        </div>
                        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats">
                                <div class="icon"><i class="fa fa-users"></i>
                                </div>
                                <div class="count"><% out.println(ricd.getInt(1)); %></div>

                                <p>Jumlah Pengguna</p>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Permintaan Kegiatan</h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    
                                             <%
                                                 int i = 0;
                                             
                                            PreparedStatement pst = conn.prepareStatement ("SELECT * FROM jadwal WHERE deleted =0 AND status=0 ORDER BY start DESC LIMIT 3");
                                            ResultSet rs = pst.executeQuery();
                                            
                                            while(rs.next())
                                            { 
                                                i++;
    SimpleDateFormat dateFormata1 = new SimpleDateFormat("MMM");
    Timestamp formata1 = rs.getTimestamp(6);
    String datea1 = dateFormata1.format(formata1);
    
    SimpleDateFormat dateFormata2 = new SimpleDateFormat("dd");
    Timestamp formata2 = rs.getTimestamp(6);
    String datea2 = dateFormata2.format(formata2);
                                            %>
                                            
                                    <article class="media event">
                                        <a class="pull-left date">
                                            <p class="month"><% out.println(datea1); %></p>
                                            <p class="day"><% out.println(datea2); %></p>
                                        </a>
                                        <div class="media-body">
                                            <a class="title" href="#"><% out.println(rs.getString(3)); %></a>
                                            <p><% out.println(rs.getString(5)); %></p>
                                            <hr>
                                            <p><small>Dari <b><% out.println(rs.getString(6)); %></b> sampai <b><% out.println(rs.getString(7)); %></b></small></p>
                                        </div>
                                    </article>
                                            <hr>
                                            <%
                                                }
                                            %>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Pengguna Baru</h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <%
                                            int j = 0;
                                             
                                            PreparedStatement pstj = conn.prepareStatement ("SELECT * FROM user WHERE deleted =0 ORDER BY waktu DESC LIMIT 5");
                                            ResultSet rsj = pstj.executeQuery();
                                            
                                            while(rsj.next())
                                            { 
                                                j++;
    SimpleDateFormat dateFormata1 = new SimpleDateFormat("MMM");
    Timestamp formata1 = rsj.getTimestamp(9);
    String datea1 = dateFormata1.format(formata1);
    
    SimpleDateFormat dateFormata2 = new SimpleDateFormat("dd");
    Timestamp formata2 = rsj.getTimestamp(9);
    String datea2 = dateFormata2.format(formata2);
                                            %>
                                            
                                    <article class="media event">
                                        <a class="pull-left date">
                                            <p class="month"><% out.println(datea1); %></p>
                                            <p class="day"><% out.println(datea2); %></p>
                                        </a>
                                        <div class="media-body">
                                            <a class="title" href="#"><% out.println(rsj.getString(3)); %> </a> <% out.println(rsj.getString(4)); %>
                                            
                                            <p><small>Bergabung pada <b><% out.println(rsj.getString(9)); %></b></small></p>
                                           
                                        </div>
                                    </article>
                                            <hr>
                                            <%
                                                }
                                            %>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>File Terbaru</h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                                                        <%
                                            int k = 0;
                                             
                                            PreparedStatement pstk = conn.prepareStatement ("SELECT * FROM share_file WHERE deleted =0 ORDER BY waktu DESC LIMIT 5");
                                            ResultSet rsk = pstk.executeQuery();
                                            
                                            while(rsk.next())
                                            { 
                                                k++;
    SimpleDateFormat dateFormata1 = new SimpleDateFormat("MMM");
    Timestamp formata1 = rsk.getTimestamp(7);
    String datea1 = dateFormata1.format(formata1);
    
    SimpleDateFormat dateFormata2 = new SimpleDateFormat("dd");
    Timestamp formata2 = rsk.getTimestamp(7);
    String datea2 = dateFormata2.format(formata2);
                                            %>
                                            
                                    <article class="media event">
                                        <a class="pull-left date">
                                            <p class="month">Nomor</p>
                                            <p class="day"><% out.println(k); %></p>
                                        </a>
                                        <div class="media-body">
                                            <a class="title" href="#"><% out.println(rsk.getString(3)); %></a>
                                            <p><% out.println(rsk.getString(6)); %></p>
                                            <p><small>Diunggah pada <b><% out.println(rsk.getString(7)); %></b></small></p>
                                           
                                        </div>
                                    </article>
                                            <hr>
                                            <%
                                                }
                                            %>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>