<%@page import="java.io.*"%>
<%@page import="com.oreilly.servlet.MultipartRequest"%>
<%@page import="java.util.Enumeration"%>
<%@page import="java.sql.PreparedStatement"%>
<%
      
    String folder = request.getParameter("folder");

            String rtempfile = File.createTempFile("temp","1").getParent();            
            MultipartRequest multi = new MultipartRequest(request,rtempfile, 15*1024*1024);     // maximum size 15 MB
            
            Enumeration files = multi.getFileNames();
            

            String st="insert into share_file(nama, file, content, user, folder, waktu, deleted) values (?,?,?,?,?,?,?)";
            PreparedStatement psmt=conn.prepareStatement(st);
            
                        
            String name="";
            String fileExtesion="";
            File ff =null;
            FileInputStream fin =null;
            
            while (files.hasMoreElements())
            {
                    name=(String)files.nextElement();                                        
                    ff = multi.getFile(name);
                    fileExtesion = ff.getName().substring(ff.getName().lastIndexOf("."));
                    
                    boolean fileAllowed = fileExtesion.equalsIgnoreCase(".txt")||
                                          fileExtesion.equalsIgnoreCase(".pdf")||
                                          fileExtesion.equalsIgnoreCase(".doc")||
                                          fileExtesion.equalsIgnoreCase(".docx")||
                                          fileExtesion.equalsIgnoreCase(".xls")||
                                          fileExtesion.equalsIgnoreCase(".xlsx");
                    
                    if((ff!=null)&&fileAllowed)
                    {

                            try
                            {
                                    fin=new FileInputStream(ff);
                                    psmt.setString(1, ff.getName());
                                    psmt.setString(2, fileExtesion);
                                    psmt.setBinaryStream(3,(InputStream)fin, (int)(ff.length()));
                                    psmt.setString(4, user_id);
                                    psmt.setString(5, folder);
                                    psmt.setString(6, now);
                                    psmt.setString(7, "0");
                                    boolean sss = psmt.execute();
                                    
                                    out.print("uploaded successfully..");
                                    out.print("<br/> Go to page");
                            }

                            catch(Exception e)
                            {
                                    out.print("Failed due to " + e);
                            }

                            finally
                            {
                            // next statement is must otherwise file will not be deleted from the temp as fin using f.
                             // its necessary to put outside otherwise at the time of exception file will not be closed.
                                    fin.close();
                                    ff.delete();
                            }
                    }
                    else
                    {
                           out.print("Please select the correct file...");
                    }// end of if and else
            }// end of while
            
            
  response.sendRedirect("share-file?folder="+folder+"");         

%>