<%
    String ubah_simpan = request.getParameter("ubah_simpan");
    
    if (ubah_simpan !=null) {
        
        String ubah_id = user_id;
        String ubah_nama = request.getParameter("nama");
        String ubah_pekerjaan = request.getParameter("pekerjaan");
        String ubah_username = request.getParameter("username");
        String ubah_email = request.getParameter("email");
        String ubah_password = request.getParameter("password");
      
      
    PreparedStatement pst = conn.prepareStatement("UPDATE user SET nama=?, pekerjaan=?, username=?, email=?, password=? WHERE id=?");
    
    pst.setString(1,ubah_nama);
    pst.setString(2,ubah_pekerjaan);
    pst.setString(3,ubah_username);
    pst.setString(4,ubah_email);
    pst.setString(5,ubah_password);
    pst.setString(6,ubah_id);
    pst.executeUpdate();       
    
    }
    
    
    PreparedStatement pstu = conn.prepareStatement("SELECT * FROM user WHERE id=?");
    pstu.setString(1,user_id);
    ResultSet rsu = pstu.executeQuery();
    
                                           
%>
               <!-- page content -->
            <div class="right_col" role="main">
                <div class="">

                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Ubah Profile User</h2>
                                    
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <br />
                                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="POST" action="">
    <%
                                                    while (rsu.next()) {
                                                        %>
                                            
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama<span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                    
                                                <input type="text" id="first-name" name="nama" required="required" value="<% out.println(rsu.getString(3)); %>" class="form-control col-md-7 col-xs-12">
                                            
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Pekerjaan<span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                               
                                                <input type="text" id="first-name" name="pekerjaan" required="required" value="<% out.println(rsu.getString(4)); %>" class="form-control col-md-7 col-xs-12">
                                            
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Username<span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="first-name" name="username" required="required" value="<% out.println(rsu.getString(5)); %>" class="form-control col-md-7 col-xs-12">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">E-mail<span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="first-name" name="email" required="required" value="<% out.println(rsu.getString(6)); %>" class="form-control col-md-7 col-xs-12">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Password<span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="password" id="first-name" name="password" required="required" value="<% out.println(rsu.getString(7)); %>" class="form-control col-md-7 col-xs-12">
                                            </div>
                                        </div>
                                            
                                                <% }%>
                                        </div>
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                <button type="submit" name="ubah_simpan" class="btn btn-success">Ubah</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /page content -->