<%-- 
    Document   : login
    Created on : Dec 22, 2015, 6:43:42 PM
    Author     : Olive
--%>
<%
//session.setAttribute("user_id", null);
if(user_id != null) {
    response.sendRedirect("index.jsp");
}
    
String login = request.getParameter("login");
String register = request.getParameter("register");
    
if(login != null) {
    String username = request.getParameter("username");
    String password = request.getParameter("password");
    String status_login = null;
    
    PreparedStatement check_login = conn.prepareStatement("SELECT * FROM user WHERE username=? AND password=? ");
    check_login.setString(1,username);
    check_login.setString(2,password);
    
    try {
        ResultSet result_login = check_login.executeQuery();
        if (result_login.next()) {
            status_login = "1";
            String result_id = result_login.getString(1);
            String result_photo = result_login.getString(2);
            String result_nama = result_login.getString(3);
            String result_username = result_login.getString(5);
            String result_autoritas = result_login.getString(8);
            
            String waktu = "2015-12-25 00:00:00";
            PreparedStatement pst = conn.prepareStatement("INSERT INTO user_session SET status='login', user_id=?, waktu=? ");

            pst.setString(1,result_id);
            pst.setString(2,waktu);
            pst.executeUpdate();
            
            
            PreparedStatement pstlp = conn.prepareStatement("SELECT * FROM user_autoritas WHERE id=?");
            pstlp.setString(1,result_autoritas);
            ResultSet rsulp = pstlp.executeQuery();
            rsulp.next();
            
            String result_jadwal_approve = rsulp.getString(3);
            String result_user_management = rsulp.getString(4);
            
            session.setAttribute("user_id", result_id);
            session.setAttribute("user_photo", result_photo);
            session.setAttribute("user_nama", result_nama);
            session.setAttribute("user_username", result_username);
            session.setAttribute("jadwal_approve", result_jadwal_approve);
            session.setAttribute("user_management", result_user_management);
            
            response.sendRedirect("index.jsp");
        }
    } catch (SQLException e) {
        status_login = null;
    }   
}

if(register != null) {
    String username = request.getParameter("username");
    String email = request.getParameter("password");
    String password = request.getParameter("password");
    String status_register = null;
    
    PreparedStatement check_login = conn.prepareStatement("SELECT * FROM user WHERE username=? OR email=? ");
    check_login.setString(1,username);
    check_login.setString(2,email);
    
    try {
        ResultSet result_login = check_login.executeQuery();
        if (result_login.next()) {
            String result_username = result_login.getString(5);
            String result_email = result_login.getString(6);
            
            if(result_username==username) {
            %>
            <script>alert("Username telah digunakan!");</script>
            <%
            }

            if(result_email==email) {
            %>
            <script>alert("Email telah digunakan!");</script>
            <%
            }
            status_register = "0";

        } else {
        String tambah_foto = "default.png";
        String tambah_nama = "New Member";
        String tambah_pekerjaan = "";
        String tambah_username = username;
        String tambah_email = email;
        String tambah_password = password;
        String tambah_autoritas = "2";
        String tambah_waktu = "2015-12-22 00:00:00";
      
        PreparedStatement pst = conn.prepareStatement("INSERT INTO user SET foto=?, nama=?, pekerjaan=?, username=?, email=?, password=?, autoritas=?, waktu=?, deleted=0");

        pst.setString(1,tambah_foto);
        pst.setString(2,tambah_nama);
       pst.setString(3,tambah_pekerjaan);
        pst.setString(4,tambah_username);
        pst.setString(5,tambah_email);
        pst.setString(6,tambah_password);
        pst.setString(7,tambah_autoritas);
        pst.setString(8,tambah_waktu);
        pst.executeUpdate();       
    
        %>
        <script>alert("Pendaftaran berhasil, silahkan Login!");</script>
        <%
        status_register = "1";
        response.sendRedirect("masuk#tologin");
        }
    } catch (SQLException e) {
       status_register = null;
    }
}
%>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>LPSI Samurai!</title>

    <!-- Bootstrap core CSS -->

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="css/custom.css" rel="stylesheet">
    <link href="css/icheck/flat/green.css" rel="stylesheet">


    <script src="js/jquery.min.js"></script>

    <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>

<body style="background:#F7F7F7;">
    
    <div class="">
        <a class="hiddenanchor" id="toregister"></a>
        <a class="hiddenanchor" id="tologin"></a>

        <div id="wrapper">
            <div id="login" class="animate form">
                <section class="login_content">
                    <form method="post" action="">
                        <h1>Masuk ke LPSI</h1>
                        <div>
                            <input name="username" type="text" class="form-control" placeholder="Username" required="" />
                        </div>
                        <div>
                            <input name="password" type="password" class="form-control" placeholder="Password" required="" />
                        </div>
                        <div>
                            <button type="submit" name="login" class="btn btn-default submit">Masuk</button>
                            <!--<a class="reset_pass" href="#">Lost your password?</a>-->
                        </div>
                        <div class="clearfix"></div>
                        <div class="separator">

                            <p class="change_link">Pengunjung baru?
                                <a href="#toregister" class="to_register"> Daftar Akun </a>
                            </p>
                            <div class="clearfix"></div>
                            <br />
                            <div>
                                <h1><i class="fa fa-university" style="font-size: 26px;margin-right: 10px;"></i>LPSI Samurai!</h1>

                                <p>�2015 All Rights Reserved.</p>
                            </div>
                        </div>
                    </form>
                    <!-- form -->
                </section>
                <!-- content -->
            </div>
            <div id="register" class="animate form">
                <section class="login_content">
                    <form method="post" action="">
                        <h1>Daftar Akun</h1>
                        <div>
                            <input name="username" type="text" class="form-control" placeholder="Username" required="" />
                        </div>
                        <div>
                            <input name="email" type="email" class="form-control" placeholder="Email" required="" />
                        </div>
                        <div>
                            <input name="password" type="password" class="form-control" placeholder="Password" required="" />
                        </div>
                        <div>
                            <button type="submit" name="register" class="btn btn-default submit">Submit</button>
                        </div>
                        <div class="clearfix"></div>
                        <div class="separator">

                            <p class="change_link">Telah menjadi member ?
                                <a href="#tologin" class="to_register"> Masuk </a>
                            </p>
                            <div class="clearfix"></div>
                            <br />
                            <div>
                                <h1><i class="fa fa-university" style="font-size: 26px;margin-right: 10px;"></i>LPSI Samurai!</h1>

                                <p>�2015 All Rights Reserved.</p>
                            </div>
                        </div>
                    </form>
                    <!-- form -->
                </section>
                <!-- content -->
            </div>
        </div>
    </div>

</body>

</html>
