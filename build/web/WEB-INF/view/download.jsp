<%-- 
    Document   : unduh
    Created on : Feb 18, 2014, 5:45:12 PM
    Author     : ghazali
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Blob"%>
<%@page import="java.io.OutputStream"%>
<%
    String doc_id = request.getParameter("id");     
    PreparedStatement pstu = conn.prepareStatement("SELECT * FROM share_file WHERE id=?");
    pstu.setString(1,doc_id);
    ResultSet rs = pstu.executeQuery();
    rs.next();
    
    response.reset();
                             
    if(rs.getString(6)==".txt")
    {
        response.setContentType("application/octet-stream");
    }
    else if(rs.getString(6)==".pdf")
    {
        response.setContentType("application/pdf");
    }
    else if((rs.getString(6)==".doc")||rs.getString(6)==".docx")
    {
        response.setContentType("application/msword");
    }
    else if((rs.getString(6)==".xls")||(rs.getString(6)==".xlsx"))
    {
        response.setContentType("application/vnd.ms-excel");
    }
    response.addHeader("Content-Disposition","attachment; filename="+rs.getString(3));
    Blob blb = rs.getBlob(5);
    byte[] bdata = blb.getBytes(1, (int) blb.length());
    
    OutputStream output =  response.getOutputStream();
    output.write(bdata);
    output.close();
    rs.close();
              
  response.sendRedirect("share-file?folder="+rs.getString(4)+"");  

%>