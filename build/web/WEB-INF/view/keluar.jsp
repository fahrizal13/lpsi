<%
if(user_id != null) {
    String status_login = null;
    
    try {
            status_login = null;
            
            String waktu = "2015-12-25 00:00:00";
            PreparedStatement pst = conn.prepareStatement("INSERT INTO user_session SET status='logout', user_id=?, waktu=? ");

            pst.setString(1,user_id);
            pst.setString(2,waktu);
            pst.executeUpdate();
            
            session.setAttribute("user_id", null);
            
            response.sendRedirect("masuk");
            
    } catch (SQLException e) {
        status_login = "1";
        response.sendRedirect("index.jsp");
    }   
} else {
  response.sendRedirect("index.jsp");
}
%>