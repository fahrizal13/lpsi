<%
    String folder = request.getParameter("folder");
    String download = request.getParameter("download");
    
    String data_id = request.getParameter("id");
    
    String hapus_file = request.getParameter("hapus_file");
    String hapus_folder = request.getParameter("hapus_folder");
    
    String tambah_file = request.getParameter("tambah_file");
    String tambah_folder = request.getParameter("tambah_folder");
    String tambah_file_simpan = request.getParameter("tambah_file_simpan");
    String tambah_folder_simpan = request.getParameter("tambah_folder_simpan");
    
    if(folder == null) { folder="0"; }
    
    if(download != null) {
        
    }
    
    if (hapus_file !=null) {
    PreparedStatement pst = conn.prepareStatement("UPDATE share_file SET deleted=1 WHERE id=?");
    pst.setString(1,data_id);
    pst.executeUpdate();
    }
    
    if (hapus_folder !=null) {
    PreparedStatement pst = conn.prepareStatement("UPDATE share_folder SET deleted=1 WHERE id=?");
    pst.setString(1,data_id);
    pst.executeUpdate();
    }
    
    if (tambah_folder_simpan !=null) {


        
    }
    
if (tambah_file !=null) {
%>
               <!-- page content -->
            <div class="right_col" role="main">
                <div class="">

                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Upload File</h2>
                                    
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <br />
                                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="POST" enctype="multipart/form-data" action="upload?folder=<% out.println(folder); %>">

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Pilih File<span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
<!--<input type="file" id="first-name" name="myupload" required="required" class="form-control col-md-7 col-xs-12">-->
<input required="required" class="form-control col-md-7 col-xs-12" type="file" name="filename" id="filename" accept=".txt, application/pdf, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/msword" />
                                            
                                            </div>
                                        </div>
                                        
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                           
                                                <button type="submit" class="btn btn-success">Upload</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /page content -->
<%           
    }
else if (tambah_folder !=null) {
%>
               <!-- page content -->
            <div class="right_col" role="main">
                <div class="">

                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Tambah Folder</h2>
                                    
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <br />
                                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="POST" action="">

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama Folder<span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                    
                                                <input type="text" id="first-name" name="nama" required="required" class="form-control col-md-7 col-xs-12">
                                            
                                            </div>
                                        </div>
                                        
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                           
                                                <button type="submit" name="tambah_folder_simpan" class="btn btn-success">Tambah</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /page content -->
<%           
    }
else {
%>
<!-- page content -->
            <div class="right_col" role="main">

                <div class="">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="x_panel">
                                <div class="x_title">
                                        <h2>Share File</h2>
                                                                        <ul class="nav navbar-right panel_toolbox">
                                        <li>
                                                        <form method="post" action="">
                                                            <button name="tambah_folder" type="submit" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i> Tambah Folder </button>
                                                        </form>
                                        </li>
                                        <li>
                                                        <form method="post" action="">
                                                            <button name="tambah_file" type="submit" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i> Upload File </button>
                                                        </form>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">

                                    <!-- start project list -->
                                    <table class="table table-striped projects">
                                        <tbody>
                                        <%
                                            PreparedStatement psta = conn.prepareStatement("SELECT * FROM share_folder WHERE deleted=0 AND sub_folder=? ORDER BY nama_folder ASC ");
                                            psta.setString(1,folder);
                                            ResultSet rsa = psta.executeQuery();
                                            while (rsa.next()) {
                                            %>
                                            <tr>
                                                <td>
                                                    <a href="share-file?folder=<% out.println(rsa.getString(1)); %>"><i class="fa fa-folder"></i> <% out.println(rsa.getString(2)); %></a>
                                                </td>
                                                <td class="text-right">
                                                <%
                                                if(rsa.getString(5).equals(user_id)) {
                                                %>
                                                        <form method="post" action="">
                                                            <input type="hidden" name="id" value ="<% out.println(rsa.getString(1)); %>" />
                                                            <button name="hapus_folder" type="submit" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Hapus </button>
                                                        </form>
                                                <%
                                                }
                                                %>
                                                </td>
                                            </tr>
                                            <% }%>
                                        <%
                                            PreparedStatement pstf = conn.prepareStatement("SELECT * FROM share_file WHERE deleted=0 AND folder=? ORDER BY nama ASC ");
                                            pstf.setString(1,folder);
                                            ResultSet rsf = pstf.executeQuery();
                                            while (rsf.next()) {
                                            %>
                                            <tr>
                                                <td>
                                                    <!--<a href="share-file?download=<% out.println(rsf.getString(1)); %>"><i class="fa fa-file-o"></i> <% out.println(rsf.getString(3)); %></a>-->
                                                    <a href="download?id=<% out.println(rsf.getString(1)); %>"><i class="fa fa-file-o"></i> <% out.println(rsf.getString(3)); %></a>
                                                </td>
                                                <td class="text-right">
                                                <%
                                                if(rsf.getString(2).equals(user_id)) {
                                                %>
                                                        <form method="post" action="">
                                                            <input type="hidden" name="id" value ="<% out.println(rsf.getString(1)); %>" />
                                                            <button name="hapus_file" type="submit" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Hapus </button>
                                                        </form>
                                                <%
                                                }
                                                %>
                                                </td>
                                            </tr>
                                            <% }%>
                                        </tbody>
                                    </table>
                                    <!-- end project list -->

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
<%           
    }
%>