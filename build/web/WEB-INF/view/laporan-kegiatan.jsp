<script type="text/javascript" src="js/jquery.min.js" > </script> 
<script type="text/javascript">

    function PrintElem(elem)
    {
        Popup($(elem).html());
    }

    function Popup(data) 
    {
        var mywindow = window.open('', 'Laporan Kegiatan', 'height=400,width=600');
        mywindow.document.write('<html><head><title>Laporan Kegiatan</title>');
        /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
        mywindow.document.write('</head><body >');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        mywindow.document.close(); // necessary for IE >= 10
        mywindow.focus(); // necessary for IE >= 10

        mywindow.print();
        mywindow.close();

        return true;
    }

</script>
<!-- page content -->
            <div class="right_col" role="main">

                <div>
                    <!--<div class="page-title">
                        <div class="title_left">
                            <h3>Jenis Kegiatan</h3>
                        </div>

                        <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                                </div>
                            </div>
                        </div>
                    </div>-->
                    <div class="clearfix"></div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Laporan Kegiatan</h2>
                                                                        <ul class="nav navbar-right panel_toolbox">
                                        <li>
                                            <button name="tambah" onclick="PrintElem('#laporanKegiatan')" class="btn btn-primary btn-xs"><i class="fa fa-print"></i> Cetak Laporan </button>
                                        </li>
                                    </ul>

                                    <!--<ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="#">Settings 1</a>
                                                </li>
                                                <li><a href="#">Settings 2</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>-->
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content" id="laporanKegiatan">

                                    <!-- start project list -->
                                    <table class="table table-striped projects">
                                        <thead>
                                            <tr>
                                                <th style="width: 1%">No.</th>
                                                <th style="width: 20%">Nama</th>
                                                <th style="width: 20%">Jenis</th>
                                                <th style="width: 20%">Author</th>
                                                <th style="width: 20%">Deskripsi</th>
                                                <th style="width: 20%">Start</th>
                                                <th style="width: 20%">Finish</th>
                                                <th style="width: 20%">Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                             <%
                                                 int i = 0;
                                             
                                            PreparedStatement pst = conn.prepareStatement ("SELECT * FROM jadwal WHERE deleted =0 ORDER BY start DESC");
                                            ResultSet rs = pst.executeQuery();
                                            
                                            while(rs.next())
                                            { 
                                                i++;
                                            %>
                                            <tr>
                                                <td><% out.println(i); %></td>
                                                <td>
                                                    <a><% out.println(rs.getString(3)); %></a>
                                                </td>
                                                <td>
<%
    PreparedStatement pstu = conn.prepareStatement("SELECT * FROM jadwal_jenis WHERE id=?");
    pstu.setString(1,rs.getString(4));
    ResultSet rsu = pstu.executeQuery();
    rsu.next();
%>
<a><% out.println(rsu.getString(2)); %></a>
                                                </td>
                                                <td>
<%
    PreparedStatement psts = conn.prepareStatement("SELECT * FROM user WHERE id=?");
    psts.setString(1,rs.getString(2));
    ResultSet rss = psts.executeQuery();
    rss.next();
%>
                                                    <a><% out.println(rss.getString(3)); %></a>
                                                </td>
                                                <td>
                                                    <a><% out.println(rs.getString(5)); %></a>
                                                </td>
                                                <td>
                                                    <a><% out.println(rs.getString(6)); %></a>
                                                </td>
                                                <td>
                                                    <a><% out.println(rs.getString(7)); %></a>
                                                </td>
                                                <td>
                                                    <a>
                                                        <%
                                                        if(rs.getString(8).equals("1")) {
                                                            out.println("Telah Disetujui");
                                                        } else {
                                                            out.println("Belum Disetujui");
                                                        }
                                                        %>
                                                    </a>
                                                </td>
                                            </tr>
                                            <%
                                                }
                                            %>
                                             </tbody>
                                    </table>
                                    <!-- end project list -->

                                </div>
                            </div>
                        </div>
                    </div>
                </div>