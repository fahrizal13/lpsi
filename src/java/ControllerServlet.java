/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author JACOW
 */
@WebServlet(urlPatterns = { "/jadwal-kegiatan",
                            "/manajemen-kegiatan", 
                            "/jenis-kegiatan", 
                            "/daftar-user", 
                            "/jenis-authoritas",
                            "/masuk",
                            "/upload",
                            "/download",
                            "/laporan-kegiatan",
                            "/profil",
                            "/keluar",
                            "/daftar-aktivitas", 
                            "/share-file"})
public class ControllerServlet extends HttpServlet {

    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {

        String userPath = request.getServletPath();
        request.setAttribute("urlpath", userPath);

        // if category page is requested
        switch (userPath) {
        // TODO: Implement category request
        // if cart page is requested
            case "/jadwal-kegiatan":
                break;
            default:
                break;
        }

        // use RequestDispatcher to forward request internally
        String url = "/WEB-INF/view" + userPath + ".jsp";
        try {
            request.getRequestDispatcher(url).forward(request, response);
        } catch (ServletException | IOException ex) {
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {

        String userPath = request.getServletPath();

        // if addToCart action is called
        switch (userPath) {
        // TODO: Implement add product to cart action
        // if updateCart action is called
            case "/jadwal-kegiatan":
                break;
            default:
                break;
        }

        // use RequestDispatcher to forward request internally
        String url = "/WEB-INF/view" + userPath + ".jsp";

        try {
            request.getRequestDispatcher(url).forward(request, response);
        } catch (ServletException | IOException ex) {
        }
    }

}